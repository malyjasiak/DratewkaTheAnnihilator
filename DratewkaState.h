//
// Created by kuba on 03.06.17.
//

#ifndef DRATEWKATHEANNIHILATOR_DRATEWKASTATE_H
#define DRATEWKATHEANNIHILATOR_DRATEWKASTATE_H
enum DratewkaState {
    AT_THE_CITY,
    LOOKING_FOR_DRAGON,
    CHASING_DRAGON,
    PLANTING_OWIECZKA,
    RUNNING_TO_CITY,
    WOUNDED
};

static string dratewkaStateToString(DratewkaState state) {
	switch(state) {
		case DratewkaState::AT_THE_CITY:
			return "W miescie";
		case DratewkaState::LOOKING_FOR_DRAGON:
			return "Szuka smoka";
		case DratewkaState::CHASING_DRAGON:
			return "Goni smoka";
		case DratewkaState::PLANTING_OWIECZKA:
			return "Podklada owieczke";
		case DratewkaState::RUNNING_TO_CITY:
			return "Wraca do miasta";
		case DratewkaState::WOUNDED :
			return "Martwy, wskrzeszanie";
		default :
			return "Nieznany";
	}
}

#endif //DRATEWKATHEANNIHILATOR_DRATEWKASTATE_H
