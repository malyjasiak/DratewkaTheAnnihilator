//
// Created by kuba on 12.05.17.
//

#ifndef DRATEWKATHEANNIHILATOR_CITY_H
#define DRATEWKATHEANNIHILATOR_CITY_H
#include <pthread.h>
#include <string>
#include "ThreadSafe.h"
#include "Printable.h"

using namespace std;
class City : public ThreadSafe, public Printable{

private:
    int hp;
    int maxHp;

public:
    City(int i);

    int getHp();
    int getMaxHp();
    bool demolish(int damage);
    bool isDemolished();

    string getPrintable();
};
#endif //DRATEWKATHEANNIHILATOR_CITY_H
