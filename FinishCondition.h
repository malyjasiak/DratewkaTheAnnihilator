//
// Created by kuba on 03.06.17.
//

#ifndef DRATEWKATHEANNIHILATOR_FINISHCONDITION_H
#define DRATEWKATHEANNIHILATOR_FINISHCONDITION_H
#include<string>
using namespace std;

enum FinishCondition {
    CITY_DEMOLISHED,
    DRAGONS_DEAD
};

static string finishConditionToString(DratewkaState state) {
	switch(state) {
		case FinishCondition::CITY_DEMOLISHED:
			return "Miasto zostało zburzone, smoki zwyciezyly!";
		case FinishCondition::DRAGONS_DEAD:
			return "Zmoki zostały zabity, miasto uratowane!";
		default :
			return "Nieznany";
	}
}

#endif //DRATEWKATHEANNIHILATOR_FINISHCONDITION_H
