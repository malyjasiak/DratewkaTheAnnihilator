//
// Created by kuba on 12.05.17.
//

#ifndef DRATEWKATHEANNIHILATOR_CAVE_H
#define DRATEWKATHEANNIHILATOR_CAVE_H

#include <pthread.h>
#include <string>
#include "ThreadSafe.h"
#include "CaveOccupationState.h"
#include "Printable.h"

using namespace std;
class Cave : public ThreadSafe, public Printable{

private:
    CaveOccupationState state;
    int id;

public:
    Cave(int id);
    bool isOccupied();
    CaveOccupationState getOccupiedBy();
    bool occupy(CaveOccupationState occupiedBy);
    void unoccupy();
    int getId();
    void setId(int id);
    string getPrintable();
};
#endif //DRATEWKATHEANNIHILATOR_CAVE_H
