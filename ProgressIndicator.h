//
// Created by kuba on 03.06.17.
//

#ifndef DRATEWKATHEANNIHILATOR_PROGRESSINDICATOR_H
#define DRATEWKATHEANNIHILATOR_PROGRESSINDICATOR_H

using namespace std;
class ProgressIndicator {
private:
    int progress = 0;
public:
    void add(int amount) { progress += amount; }
    void reset() { progress = 0; }
    int getProgress() { return this->progress; }
};
#endif //DRATEWKATHEANNIHILATOR_PROGRESSINDICATOR_H
