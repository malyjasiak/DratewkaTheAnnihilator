//
// Created by kuba on 12.05.17.
//

#ifndef DRATEWKATHEANNIHILATOR_DRATEWKA_H
#define DRATEWKATHEANNIHILATOR_DRATEWKA_H
#define DRATEWKA_MAX_SPEED 20
#include<string>
#include <vector>
#include <list>
#include "Cave.h"
#include "City.h"
#include "ProgressIndicator.h"
#include "DratewkaState.h"
#include "Printable.h"

using namespace std;

class Dratewka : public ProgressIndicator, public Printable, public ThreadSafe{

private:
    int id;
    DratewkaState state;
    uint seed;
public:
    Dratewka(int id);
    uint * getSeed();
    int getId();
    void setId(int id);
    DratewkaState getState();
    void setState(DratewkaState state);
    string getPrintable();
};
#endif //DRATEWKATHEANNIHILATOR_DRATEWKA_H
